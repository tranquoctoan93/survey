//
//  Utils.swift
//  SurveysApp
//
//  Created by Toof_Appsia on 7/30/17.
//  Copyright © 2017 Toof_Appsia. All rights reserved.
//

import XCTest
import RealmSwift
import Alamofire
import ObjectMapper
import RealmS
@testable import SurveysApp

extension Dictionary where Key: ExpressibleByStringLiteral, Value: Any {
    static func jsonFromFile(bundle: Bundle, fileName: String) -> JSArray? {
        let jsonData = try? Data(contentsOf: URL(fileURLWithPath: bundle.path(forResource: fileName, ofType: "json")!))
        if let jsonData = jsonData {
            return jsonFromData(jsonData)
        }
        return nil
    }
    
    static func jsonFromData(_ data: Data) -> JSArray? {
        do {
            return try JSONSerialization.jsonObject(with: data, options: []) as? JSArray
        }
        catch {
            return nil
        }
    }
}
