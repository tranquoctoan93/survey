//
//  TransformTests.swift
//  SurveysApp
//
//  Created by Toof_Appsia on 7/30/17.
//  Copyright © 2017 Toof_Appsia. All rights reserved.
//

import XCTest
import RealmSwift
import Alamofire
import ObjectMapper
import RealmS
import SwiftUtils
@testable import SurveysApp

class TransformTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testTransform() {
        let jsonValue = ["en", "vn", "fr", "sp"]
        if let listRealmString = ListRealmStringTransform().transformFromJSON(jsonValue) {
            XCTAssertEqual(listRealmString.count, jsonValue.count)
            
            for (index, realmString) in listRealmString.enumerated() {
                XCTAssertEqual(realmString.content, jsonValue[index])
            }
            
            if let value = ListRealmStringTransform().transformToJSON(listRealmString) {
                XCTAssertEqual(value, jsonValue)
            }
        }
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
