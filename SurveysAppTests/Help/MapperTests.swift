//
//  MapperTests.swift
//  SurveysApp
//
//  Created by Toof_Appsia on 7/30/17.
//  Copyright © 2017 Toof_Appsia. All rights reserved.
//

import XCTest
import RealmSwift
import Alamofire
import ObjectMapper
import RealmS
import SwiftUtils
@testable import SurveysApp

class MapperTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testMap() {
        // File response.json contain response with page = 1 and per_page = 1
        let fileName = "response"
        let bundle = Bundle(for: type(of: self))
        if let jsonArray = Dictionary<String, Any>.jsonFromFile(bundle: bundle, fileName: fileName) {
            
            // Clear database
            Survey.clean()
            
            Mapper<Survey>().map(result: Result<Any>.success(jsonArray), type: .array, completion: { (result) in
                switch result {
                case .success(_):
                    let surveys = RealmS().objects(Survey.self)
                    XCTAssertEqual(surveys.count, 1)
                    if let objects = Mapper<Survey>().mapArray(JSONArray: jsonArray) {
                        XCTAssertEqual(objects.count, 1)
                        guard let object = objects.first, let survey = surveys.first else { return }
                        XCTAssertEqual(object.id, survey.id)
                        XCTAssertEqual(object.title, survey.title)
                        XCTAssertEqual(object.surveyDescription, survey.surveyDescription)
                    }
                case .failure(let error):
                    XCTFail(error.localizedDescription)
                }
            })
        }
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
