//
//  FontTests.swift
//  SurveysApp
//
//  Created by Toof_Appsia on 7/29/17.
//  Copyright © 2017 Toof_Appsia. All rights reserved.
//

import XCTest
@testable import SurveysApp

class FontTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testFetchFont() {
        let avenirMediumFontString = "Avenir-Medium"
        let font = UIFont.font(.Avenir, type: .Medium, size: 17.0)
        XCTAssertEqual(font?.fontName, avenirMediumFontString)
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
