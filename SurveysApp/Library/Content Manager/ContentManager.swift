//
//  ContentManager.swift
//  SurveysApp
//
//  Created by Toof_Appsia on 7/26/17.
//  Copyright © 2017 Toof_Appsia. All rights reserved.
//

import Foundation

final class ContentManager {
    static var shareInstance = ContentManager()
    var stringCache: [String: String]
    
    init() {
        self.stringCache = [String: String]()
    }
    
    func stringForKey(_ key: String) -> String! {
        var string = self.stringCache[key]
        if string == nil {
            string = Bundle.main.localizedString(forKey: key, value: nil, table: nil)
            self.stringCache[key] = string
        }
        return string
    }
}
