//
//  Transform.swift
//  SurveysApp
//
//  Created by Toof_Appsia on 7/26/17.
//  Copyright © 2017 Toof_Appsia. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper

class ListRealmStringTransform: TransformType {
    typealias Object = List<RealmString>
    typealias JSON = Array<String>
    
    func transformFromJSON(_ value: Any?) -> Object? {
        let results = Object()
        if let values = value as? JSON {
            for value in values {
                let realmString = RealmString()
                realmString.content = value
                results.append(realmString)
            }
        }
        return results
    }
    
    func transformToJSON(_ value: Object?) -> JSON? {
        var results = JSON()
        if let values = value {
            for value in values {
                results.append(value.content)
            }
        }
        return results
    }
}
