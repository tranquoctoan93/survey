//
//  Hud.swift
//  SurveysApp
//
//  Created by Toof_Appsia on 7/26/17.
//  Copyright © 2017 Toof_Appsia. All rights reserved.
//

import UIKit
import SVProgressHUD

extension SVProgressHUD {
    static func loading() {
        self.show()
        self.setForegroundColor(UIColor.white)
        self.setBackgroundColor(UIColor.black.withAlphaComponent(0.7))
        self.setDefaultMaskType(.clear)
    }
}
