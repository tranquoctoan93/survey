//
//  Kingfisher.swift
//  SurveysApp
//
//  Created by Toof_Appsia on 7/26/17.
//  Copyright © 2017 Toof_Appsia. All rights reserved.
//

import UIKit
import Kingfisher

extension UIImageView {
    func loadImageFromURL(_ url: String!){
        if url == nil || url == "" {
            return
        }
        self.kf.setImage(with: URL(string: url), placeholder: UIImage(named: "empty-image"), options: nil, progressBlock: nil, completionHandler: nil)
    }
    
    func loadImageHeightFromURL(_ url: String!){
        if url == nil || url == "" {
            return
        }
        self.kf.setImage(with: URL(string: url + "l"), placeholder: UIImage(named: "empty-image"), options: nil, progressBlock: nil, completionHandler: nil)
    }
    
    func cancelRequestImage() {
        self.kf.cancelDownloadTask()
    }
}

