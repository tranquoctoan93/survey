//
//  Font.swift
//  SurveysApp
//
//  Created by Toof_Appsia on 7/25/17.
//  Copyright © 2017 Toof_Appsia. All rights reserved.
//

import UIKit

enum FontType : String {
    case Bold             = "-Bold"
    case DemiBold         = "-DemiBold"
    case Light            = "-Light"
    case Medium           = "-Medium"
    case Regular          = "-Regular"
    case None             = ""
}

enum FontName : String {
    case Avenir = "Avenir"
    case AvenirNext = "AvenirNext"
}

enum FontScale: CGFloat {
    case iPhone = 1.0
    case iPad = 1.135
}

let fontScale: FontScale = ((UIDevice.current.userInterfaceIdiom == .phone
    ) ? .iPhone : .iPad)

extension UIFont {
    class func font(_ name: FontName, type: FontType, size: CGFloat) -> UIFont! {
        let fontName = name.rawValue + type.rawValue
        let fontSize = size * fontScale.rawValue
        let font = UIFont(name: fontName, size: fontSize)
        if let font = font {
            return font
        } else {
            return UIFont.systemFont(ofSize: fontSize)
        }
    }
    
    class func titleTextFont() -> UIFont! {
        return UIFont.font(.Avenir, type: .Medium, size: 17.0)
    }
    
    class func takeButtonTextFont() -> UIFont! {
        return UIFont.font(.Avenir, type: .Medium, size: 21.0)
    }
    
    class func surveyDescriptionTextFont() -> UIFont! {
        return UIFont.font(.AvenirNext, type: .Medium, size: 17.0)
    }
    
    class func surveyTitleTextFont() -> UIFont! {
        return UIFont.font(.AvenirNext, type: .Bold, size: 31.0)
    }
}

