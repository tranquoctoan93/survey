//
//  Color.swift
//  SurveysApp
//
//  Created by Toof_Appsia on 7/25/17.
//  Copyright © 2017 Toof_Appsia. All rights reserved.
//

import UIKit

extension UIColor {
    class var navigationColor: UIColor {
        return UIColor(red: 19.0 / 255.0, green: 27.0 / 255.0, blue: 49.0 / 255.0, alpha: 1.0)
    }
    
    class var takeButtonColor: UIColor {
        return UIColor(red: 225.0 / 255.0, green: 65.0 / 255.0, blue: 78.0 / 255.0, alpha: 1.0)
    }
}

