//
//  Mapper.swift
//  SurveysApp
//
//  Created by Toof_Appsia on 7/25/17.
//  Copyright © 2017 Toof_Appsia. All rights reserved.
//

import Foundation
import Alamofire
import RealmSwift
import ObjectMapper
import RealmS

enum DataType {
    case object
    case array
}

extension Mapper where N: Object, N: BaseMappable {
    func map(result: Result<Any>, type: DataType, completion: Completion) {
        switch result {
        case .success(let json):
            switch type {
            case .object:
                guard let obj = json as? JSObject else {
                    completion(.failure(Api.Error.json))
                    return
                }
                let realm = RealmS()
                realm.write {
                    realm.map(N.self, json: obj)
                }
                completion(.success(json))
            case .array:
                guard let objs = json as? JSArray else {
                    completion(.failure(Api.Error.json))
                    return
                }
                let realm = RealmS()
                realm.write {
                    realm.map(N.self, json: objs)
                }
                completion(.success(json))
            }
        case .failure(let error):
            completion(.failure(error))
        }
    }
}
