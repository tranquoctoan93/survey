//
//  String.swift
//  SurveysApp
//
//  Created by Toof_Appsia on 7/25/17.
//  Copyright © 2017 Toof_Appsia. All rights reserved.
//

import UIKit

extension String {
    var host: String? { return (try? asURL())?.host }
    
    static func stringAttributes(_ font: UIFont, color: UIColor, paragraph: NSMutableParagraphStyle? = NSMutableParagraphStyle()) -> [String: Any]? {
        return [NSFontAttributeName: font,
                NSForegroundColorAttributeName: color,
                NSParagraphStyleAttributeName: paragraph ?? NSMutableParagraphStyle()
        ]
    }
}
