//
//  GlobalConfig.swift
//  SurveysApp
//
//  Created by Toof_Appsia on 7/25/17.
//  Copyright © 2017 Toof_Appsia. All rights reserved.
//

import Foundation
import SVProgressHUD

// UI
typealias hud = SVProgressHUD

// Content String Manager
let contentManager = ContentManager.shareInstance
