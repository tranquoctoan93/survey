//
//  MainViewController.swift
//  SurveysApp
//
//  Created by Toof_Appsia on 7/25/17.
//  Copyright © 2017 Toof_Appsia. All rights reserved.
//

import UIKit
import SwiftUtils
import RealmS
import RealmSwift

class MainViewController: BaseViewController {
    @IBOutlet weak var collectionView: UICollectionView!
    var surveys: List<Survey> = List<Survey>()
    var verticalPageControl: VerticalPageControl!
    var minimumSpacing: CGFloat = 0.0
    let widthVerticalPageControl: CGFloat = 39.0
    var page = 1
    var perPage = 10

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func setup() {
        super.setup()
        self.fetchData()
        self.setupPageControl()
        self.setupCollectionView()
    }
    
    override func screenTitle() -> String {
        return contentManager.stringForKey("surveyTitle")
    }
    
    override func rightButtonItem() -> UIBarButtonItem! {
        return UIBarButtonItem(image: UIImage(named: "ic-menu"), style: .done, target: self, action: #selector(menu(_:)))
    }
    
    override func leftButtonItem() -> UIBarButtonItem! {
        return UIBarButtonItem(image: UIImage(named: "ic-refresh"), style: .done, target: self, action: #selector(refresh(_:)))
    }
    
    func setupPageControl() {
        self.verticalPageControl = VerticalPageControl()
        self.view.addSubview(self.verticalPageControl)
        self.view.addConstraintsWithFormat("V:|[v0]|", views: self.verticalPageControl)
        self.view.addConstraintsWithFormat("H:[v0(\(self.widthVerticalPageControl))]|", views: self.verticalPageControl)
    }
    
    func setupCollectionView() {
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        self.collectionView.registerNib(SurveyCardCollectionViewCell.self)
        let folowLayout = UICollectionViewFlowLayout()
        folowLayout.minimumLineSpacing = self.minimumSpacing
        folowLayout.minimumInteritemSpacing = self.minimumSpacing
        folowLayout.scrollDirection = .vertical
        self.collectionView.collectionViewLayout = folowLayout
        self.collectionView.isPagingEnabled = true
        self.collectionView.showsVerticalScrollIndicator = false
    }
    
    deinit {
        api.cancel()
    }
}

    //  MARK: - Handle action

extension MainViewController {
    func refresh(_ sender: UIBarButtonItem) {
        api.cancel()
        self.fetchData()
    }
    
    func menu(_ sender: UIBarButtonItem) {
        print("Menu... ")
    }
}

    //  MARK: - Handle UIScrollViewDelegate

extension MainViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView is UICollectionView {
            let total = scrollView.contentSize.height - scrollView.bounds.size.height
            let offset = scrollView.contentOffset.y
            let percent = Double(offset / total)
            self.verticalPageControl.progress = percent * Double(self.surveys.count - 1)
        }
    }
}

    //  MARK: - Handle UICollectionViewDataSource

extension MainViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.surveys.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let survey = self.surveys[indexPath.item]
        let cell = collectionView.dequeue(SurveyCardCollectionViewCell.self, forIndexPath: indexPath)
        cell?.survey = survey
        cell?.coverImageView.loadImageHeightFromURL(survey.coverImageUrl)
        cell?.nameLabel.text = survey.title
        cell?.descriptionLabel.text = survey.surveyDescription
        cell?.delegate = self
        return cell!
    }
}

    //  MARK: - Handle UICollectionViewDelegateFlowLayout

extension MainViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return self.collectionView.frame.size
    }
}

    //  MARK: - Handle TakeSurveyDelegate

extension MainViewController: TakeSurveyDelegate {
    func takeSurvey(_ survey: Survey) {
        let takeSurveyVC = TakeTheSurveyViewController.createViewControllerFromNib()
        self.navigationController?.pushViewController(takeSurveyVC, animated: true)
    }
}

    //  MARK: - Handle Fetch Data

extension MainViewController {
    func fetchData() {
        hud.loading()
        let params = Api.Survey.QueryParams(page: self.page, perPage: self.perPage)
        Api.Survey.query(params: params) { [weak self] (result) in
            guard let `self` = self else { return }
            RealmS().refresh()
            switch result {
            case .success(_):
                
                // To do somthing if request success
                
                break
            case .failure(let error):
                self.alert(error: error)
                break
            }
            self.fetchDataFromDatabase()
            hud.dismiss()
        }
    }
    
    func fetchDataFromDatabase() {
        self.surveys = RealmS().objects(Survey.self).reduce(List<Survey>(), { (list, element) -> List<Survey> in
            list.append(element)
            return list
        })
        self.verticalPageControl.numberOfPages = self.surveys.count
        self.collectionView.reloadData()
    }
}
