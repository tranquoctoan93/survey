//
//  TakeTheSurveyViewController.swift
//  SurveysApp
//
//  Created by Toof_Appsia on 7/26/17.
//  Copyright © 2017 Toof_Appsia. All rights reserved.
//

import UIKit
import SwiftUtils

class TakeTheSurveyViewController: BaseViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func setup() {
        super.setup()
    }
    
    override func screenTitle() -> String {
        return contentManager.stringForKey("takeTheSurveyTitle")
    }
    
    override func leftButtonItem() -> UIBarButtonItem! {
        return UIBarButtonItem(image: UIImage(named: "ic-back"), style: .done, target: self, action: #selector(back(_:)))
    }
}

    //  MARK: - Handle action

extension TakeTheSurveyViewController {
    func back(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
}
