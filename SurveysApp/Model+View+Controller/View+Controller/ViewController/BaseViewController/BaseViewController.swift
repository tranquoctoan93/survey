//
//  BaseViewController.swift
//  SurveysApp
//
//  Created by Toof_Appsia on 7/25/17.
//  Copyright © 2017 Toof_Appsia. All rights reserved.
//

import UIKit
import SwiftUtils

class BaseViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.title = screenTitle()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        hud.dismiss()
    }
    
    func setup() {
        self.setupRightButton()
        self.setupLeftButton()
    }
    
    func screenTitle() -> String {
        return ""
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}

    //  MARK: - UIBarButtonItem

extension BaseViewController {
    func rightButtonItem() -> UIBarButtonItem! {
        return nil
    }
    
    func leftButtonItem() -> UIBarButtonItem! {
        return nil
    }
    
    fileprivate func setupRightButton() {
        self.navigationItem.rightBarButtonItem = self.rightButtonItem()
    }
    
    fileprivate func setupLeftButton() {
        self.navigationItem.leftBarButtonItem = self.leftButtonItem()
    }
}
