//
//  PageControlVertical.swift
//  SurveysApp
//
//  Created by Toof_Appsia on 7/27/17.
//  Copyright © 2017 Toof_Appsia. All rights reserved.
//

import UIKit
import CHIPageControl

class VerticalPageControl: CHIPageControlChimayo {
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setup()
    }
    
    func setup() {
        self.transform = CGAffineTransform(rotationAngle: CGFloat.pi/2)
        self.radius = 6.5
        self.padding = 10.0
        self.tintColor = UIColor.white
    }
}
