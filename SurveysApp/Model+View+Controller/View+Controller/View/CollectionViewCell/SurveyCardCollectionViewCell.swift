//
//  SurveyCardCollectionViewCell.swift
//  SurveysApp
//
//  Created by Toof_Appsia on 7/26/17.
//  Copyright © 2017 Toof_Appsia. All rights reserved.
//

import UIKit

protocol TakeSurveyDelegate: class {
    func takeSurvey(_ survey: Survey)
}

class SurveyCardCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var takeButton: UIButton!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var coverImageView: UIImageView!
    weak var delegate: TakeSurveyDelegate?
    var survey: Survey!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.setup()
    }
    
    func setup() {
        self.setupTakeButton()
        self.setupNameLabel()
        self.setupDescriptionLabel()
    }
    
    func setupNameLabel() {
        self.nameLabel.font = UIFont.surveyTitleTextFont()
        self.nameLabel.textColor = UIColor.white
        self.nameLabel.textAlignment = .center
        self.nameLabel.numberOfLines = 0
        self.nameLabel.lineBreakMode = .byWordWrapping
    }
    
    func setupDescriptionLabel() {
        self.descriptionLabel.font = UIFont.surveyDescriptionTextFont()
        self.descriptionLabel.textColor = UIColor.white.withAlphaComponent(0.55)
        self.descriptionLabel.textAlignment = .center
    }
    
    func setupTakeButton() {
        self.takeButton.backgroundColor = UIColor.takeButtonColor
        self.takeButton.setTitle(contentManager.stringForKey("takeButtonTitle"), for: .normal)
        self.takeButton.tintColor = UIColor.white
        self.takeButton.titleLabel?.font = UIFont.takeButtonTextFont()
        self.takeButton.layer.cornerRadius = self.takeButton.frame.height / 2
        self.takeButton.addTarget(self, action: #selector(takeSurvey(_:)), for: .touchUpInside)
    }
}

    //  MARK: - Handle action 

extension SurveyCardCollectionViewCell {
    func takeSurvey(_ sender: UIButton) {
        delegate?.takeSurvey(self.survey)
    }
}
