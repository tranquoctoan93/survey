//
//  API.Survey.swift
//  SurveysApp
//
//  Created by Toof_Appsia on 7/25/17.
//  Copyright © 2017 Toof_Appsia. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

extension Api.Survey {
    struct QueryParams {
        let accessToken: String = "0335cf1751fdfaa4759393dcbb8ce1db4bf679d182db905a5867e8fad8958696"
        let page: Int
        let perPage: Int
        
        func toJson() -> [String: Any] {
            return [
                "access_token": accessToken,
                "page": page,
                "per_page": perPage
            ]
        }
    }
    
    @discardableResult
    static func query(params: Api.Survey.QueryParams, completion: @escaping Completion) -> Request? {
        let path = Api.Path.Survey.path
        return api.request(method: .get, urlString: path, parameters: params.toJson(), completion: { (result) in
            if result.isSuccess {
                Survey.clean()
            }
            switch result {
            case .success(let json):
                guard let objs = json as? JSArray else {
                    DispatchQueue.main.async {
                        completion(.failure(Api.Error.json))
                    }
                    return
                }
                Mapper<Survey>().map(result: .success(objs), type: .array, completion: { (result) in
                    api.request = nil
                    DispatchQueue.main.async {
                        completion(result)
                    }
                })
                break
            case .failure(let error):
                DispatchQueue.main.async {
                    completion(.failure(error))
                }
                break
            }
        })
    }
}
