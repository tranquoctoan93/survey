//
//  ApiManager.swift
//  SurveysApp
//
//  Created by Toof_Appsia on 7/25/17.
//  Copyright © 2017 Toof_Appsia. All rights reserved.
//

import Foundation
import Alamofire

typealias JSObject = [String: Any]
typealias JSArray = [JSObject]

typealias Completion = (Result<Any>) -> Void

let api = ApiManager()

    // MARK: - Services API Manager define
final class ApiManager {
    var request: DataRequest?
    
    deinit {
        self.cancel()
        self.request = nil
    }
}
