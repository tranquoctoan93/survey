//
//  Api.swift
//  SurveysApp
//
//  Created by Toof_Appsia on 7/25/17.
//  Copyright © 2017 Toof_Appsia. All rights reserved.
//

import Foundation

final class Api {
    struct Path {
        static let baseURL = "https://nimbl3-survey-api.herokuapp.com"
    }

    // MARK: - Services properties define
    struct Survey {
    }
}

extension Api.Path {
    struct Survey: ApiPath {
        static var path: String { return baseURL / "surveys" }
        let id: String
        var urlString: String { return Survey.path / id }
    }
}

protocol URLStringConvertible {
    var urlString: String { get }
}

protocol ApiPath: URLStringConvertible {
    static var path: String { get }
}

extension URL: URLStringConvertible {
    var urlString: String { return absoluteString }
}

extension Int: URLStringConvertible {
    var urlString: String { return String(describing: self) }
}

fileprivate func / (lhs: URLStringConvertible, rhs: URLStringConvertible) -> String {
    return lhs.urlString + "/" + rhs.urlString
}

extension String: URLStringConvertible {
    var urlString: String { return self }
}

extension CustomStringConvertible where Self: URLStringConvertible {
    var urlString: String { return description }
}
