//
//  RequestSerializer.swift
//  SurveysApp
//
//  Created by Toof_Appsia on 7/25/17.
//  Copyright © 2017 Toof_Appsia. All rights reserved.
//

import Alamofire
import Foundation

extension ApiManager {
    @discardableResult
    func request(method: HTTPMethod,
                 urlString: URLStringConvertible,
                 parameters: [String: Any]? = nil,
                 headers: [String: String]? = nil,
                 completion: @escaping Completion) -> Request? {

        guard Network.shared.isReachable else {
            completion(.failure(Api.Error.network))
            return nil
        }

        let encoding: ParameterEncoding
        if method == .post {
            encoding = JSONEncoding.default
        } else {
            encoding = URLEncoding.default
        }

        self.request = Alamofire.request(urlString.urlString,
                                        method: method,
                                        parameters: parameters,
                                        encoding: encoding
        ).responseJSON(completion: { (response) in
            completion(response.result)
        })

        return self.request
    }
    
    func cancel() {
        self.request?.cancel()
    }
    
    func pause() {
        self.request?.suspend()
    }
    
    func resume() {
        self.request?.resume()
    }
}
