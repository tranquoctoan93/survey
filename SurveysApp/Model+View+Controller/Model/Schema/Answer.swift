//
//  Answer.swift
//  SurveysApp
//
//  Created by Toof_Appsia on 7/25/17.
//  Copyright © 2017 Toof_Appsia. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper
import RealmS

final class Answer: Object, Mappable {
    dynamic var id: String = ""
    dynamic var questionId: String = ""
    dynamic var text: String = ""
    dynamic var helpText: String?
    dynamic var inputMaskPlaceholder: String?
    dynamic var shortText: String = ""
    dynamic var isMandatory: Bool = false
    dynamic var isCustomerFirstName: Bool = false
    dynamic var isCustomerLastName: Bool = false
    dynamic var isCustomerTitle: Bool = false
    dynamic var isCustomerEmail: Bool = false
    dynamic var promptCustomAnswer: Bool = false
    dynamic var weight: String = ""
    dynamic var displayOrder: Int = 0
    dynamic var displayType: String = ""
    dynamic var inputMask: String?
    dynamic var dateConstraint: Date?
    dynamic var defaultValue: String?
    dynamic var responseClass: String = ""
    dynamic var referenceIdentifier: String?
    dynamic var score: Int = 0
    var alerts = List<RealmString>()
    let questions = LinkingObjects(fromType: Question.self, property: "answers")
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        id                      <-  map["id"]
        questionId              <-  map["question_id"]
        text                    <-  map["text"]
        helpText                <-  map["help_text"]
        inputMaskPlaceholder    <-  map["input_mask_placeholder"]
        shortText               <-  map["short_text"]
        isMandatory             <-  map["is_mandatory"]
        isCustomerFirstName     <-  map["is_customer_first_name"]
        isCustomerLastName      <-  map["is_customer_last_name"]
        isCustomerTitle         <-  map["is_customer_title"]
        isCustomerEmail         <-  map["is_customer_email"]
        promptCustomAnswer      <-  map["prompt_custom_answer"]
        weight                  <-  map["weight"]
        displayOrder            <-  map["display_order"]
        displayType             <-  map["display_type"]
        inputMask               <-  map["input_mask"]
        dateConstraint          <-  (map["date_constraint"], DateTransform())
        defaultValue            <-  map["default_value"]
        responseClass           <-  map["response_class"]
        referenceIdentifier     <-  map["reference_identifier"]
        score                   <-  map["score"]
        alerts                  <-  (map["alerts"], ListRealmStringTransform())
    }
}

extension Answer {
    override class func relativedTypes() -> [Object.Type] {
        return [RealmString.self]
    }
    
    override class func clean() {
        let realm = RealmS()
        let objs = realm.objects(self).filter("questions.@count = 0")
        realm.write {
            realm.delete(objs)
        }
    }
}
