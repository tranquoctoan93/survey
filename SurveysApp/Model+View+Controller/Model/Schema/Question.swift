//
//  Question.swift
//  SurveysApp
//
//  Created by Toof_Appsia on 7/25/17.
//  Copyright © 2017 Toof_Appsia. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper
import RealmS

final class Question: Object, Mappable {
    dynamic var id: String = ""
    dynamic var text: String = ""
    dynamic var helpText: String?
    dynamic var displayOrder: Int = 0
    dynamic var shortText: String?
    dynamic var pick: String = ""
    dynamic var displayType: String = ""
    dynamic var isMandatory: Bool = false
    dynamic var correctAnswerId: String?
    dynamic var facebookProfile: String?
    dynamic var twitterProfile: String?
    dynamic var imageUrl: String = ""
    dynamic var coverImageUrl: String = ""
    dynamic var coverImageOpacity: Double = 0.0
    dynamic var coverBackgroundColor: String?
    dynamic var isShareableOnFacebook: Bool = false
    dynamic var isShareableOnTwitter: Bool = false
    dynamic var fontFace: String?
    var fontSize = RealmOptional<Double>()
    dynamic var tagList: String = ""
    let answers: List<Answer> = List<Answer>()
    let surveys = LinkingObjects(fromType: Survey.self, property: "questions")
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        id                      <-  map["id"]
        text                    <-  map["text"]
        helpText                <-  map["help_text"]
        displayOrder            <-  map["display_order"]
        shortText               <-  map["short_text"]
        pick                    <-  map["pick"]
        displayType             <-  map["display_type"]
        isMandatory             <-  map["is_mandatory"]
        correctAnswerId         <-  map["correct_answer_id"]
        facebookProfile         <-  map["facebook_profile"]
        twitterProfile          <-  map["twitter_profile"]
        imageUrl                <-  map["image_url"]
        coverImageUrl           <-  map["cover_image_url"]
        coverImageOpacity       <-  map["cover_image_opacity"]
        coverBackgroundColor    <-  map["cover_background_color"]
        isShareableOnFacebook   <-  map["is_shareable_on_facebook"]
        isShareableOnTwitter    <-  map["is_shareable_on_twitter"]
        fontFace                <-  map["font_face"]
        fontSize                <-  map["font_size"]
        tagList                 <-  map["tag_list"]
        answers                 <-  map["answers"]
    }
}

extension Question {
    override class func relativedTypes() -> [Object.Type] {
        return [Answer.self]
    }
    
    override class func clean() {
        let realm = RealmS()
        let objs = realm.objects(self).filter("surveys.@count = 0")
        realm.write {
            realm.delete(objs)
        }
    }
}
