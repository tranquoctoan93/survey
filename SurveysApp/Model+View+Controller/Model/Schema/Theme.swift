//
//  Theme.swift
//  SurveysApp
//
//  Created by Toof_Appsia on 7/25/17.
//  Copyright © 2017 Toof_Appsia. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper
import RealmS

final class Theme: Object, Mappable {
    dynamic var colorActive: String = ""
    dynamic var colorInactive: String = ""
    dynamic var colorQuestion: String = ""
    dynamic var colorAnswerNormal: String = ""
    dynamic var colorAnswerInactive: String = ""
    let surveys = LinkingObjects(fromType: Survey.self, property: "theme")
    
    convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        colorActive         <- map["color_active"]
        colorInactive       <-  map["color_inactive"]
        colorQuestion       <-  map["color_question"]
        colorAnswerNormal   <-  map["color_answer_normal"]
        colorAnswerInactive <-  map["color_answer_inactive"]
    }
}

extension Theme {
    override class func clean() {
        let realm = RealmS()
        let objs = realm.objects(self).filter("surveys.@count = 0")
        realm.write {
            realm.delete(objs)
        }
    }
}
