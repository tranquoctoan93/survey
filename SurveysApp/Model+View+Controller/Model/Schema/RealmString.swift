//
//  RealmString.swift
//  SurveysApp
//
//  Created by Toof_Appsia on 7/26/17.
//  Copyright © 2017 Toof_Appsia. All rights reserved.
//

import Foundation
import RealmSwift
import RealmS

class RealmString: Object {
    dynamic var content: String = ""
    let languageList = LinkingObjects(fromType: Survey.self, property: "languageList")
    let alerts = LinkingObjects(fromType: Answer.self, property: "alerts")
    
    override static func primaryKey() -> String? {
        return "content"
    }
}

extension RealmString {
    override class func clean() {
        let realm = RealmS()
        let objs = realm.objects(self).filter("languageList.@count = 0 && alerts.@count = 0")
        realm.write {
            realm.delete(objs)
        }
    }
}
