//
//  Survey.swift
//  SurveysApp
//
//  Created by Toof_Appsia on 7/25/17.
//  Copyright © 2017 Toof_Appsia. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper
import RealmS

final class Survey: Object, Mappable {
    dynamic var id: String = ""
    dynamic var title: String = ""
    dynamic var surveyDescription: String = ""
    dynamic var accessCodePrompt: String = ""
    dynamic var thankEmailAboveThreshold: String = ""
    dynamic var footerContent: String = ""
    dynamic var isActive: Bool = false
    dynamic var coverImageUrl: String = ""
    dynamic var coverBackgroundColor: String?
    dynamic var type: String = ""
    dynamic var createdAt: Date?
    dynamic var activeAt: Date?
    dynamic var inactiveAt: Date?
    dynamic var surveyVersion: Int = 0
    dynamic var shortUrl: String = ""
    var languageList = List<RealmString>()
    dynamic var defaultLanguage: String = ""
    dynamic var tagList: String = ""
    dynamic var isAccessCodeRequired: Bool = false
    dynamic var isAccessCodeValidRequired: Bool = false
    dynamic var accessCodeValidation: String = ""
    dynamic var theme: Theme?
    let questions: List<Question> = List<Question>()
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        id                          <-  map["id"]
        title                       <-  map["title"]
        surveyDescription           <-  map["description"]
        accessCodePrompt            <-  map["access_code_prompt"]
        thankEmailAboveThreshold    <-  map["thank_email_above_threshold"]
        footerContent               <-  map["footer_content"]
        isActive                    <-  map["is_active"]
        coverImageUrl               <-  map["cover_image_url"]
        coverBackgroundColor        <-  map["cover_background_color"]
        type                        <-  map["type"]
        createdAt                   <-  (map["created_at"], DateTransform())
        activeAt                    <-  (map["active_at"], DateTransform())
        inactiveAt                  <-  (map["inactive_at"], DateTransform())
        surveyVersion               <-  map["survey_version"]
        shortUrl                    <-  map["short_url"]
        languageList                <-  (map["language_list"], ListRealmStringTransform())
        defaultLanguage             <-  map["default_language"]
        tagList                     <-  map["tag_list"]
        isAccessCodeRequired        <-  map["is_access_code_required"]
        isAccessCodeValidRequired   <-  map["is_access_code_valid_required"]
        accessCodeValidation        <-  map["access_code_validation"]
        theme                       <-  map["theme"]
        questions                   <-  map["questions"]
    }
}

extension Survey {
    override class func relativedTypes() -> [Object.Type] {
        return [Theme.self, Question.self, RealmString.self]
    }
    
    override class func clean() {
        let realm = RealmS()
        let objs = realm.objects(self)
        realm.write {
            realm.delete(objs)
        }
    }
}
